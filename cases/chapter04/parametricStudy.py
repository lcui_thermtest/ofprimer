#!/usr/bin/env python

# PyFoam modules for manipulating OpenFOAM files 
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from PyFoam.Basics.DataStructures import Vector

# Commandline arguments and paths
import os
import sys
import argparse

# Parameter variation 
import numpy as np
from numpy import linspace,sin,cos,array,pi
import yaml

def rot_matrix(angle_deg):
    angle_rad = angle_deg * pi / 180.
    return np.array([[cos(angle_rad),0,-sin(angle_rad)],
                     [0,1,0],
                     [sin(angle_rad), 0, cos(angle_rad)]])

def rot_vector(vec, angle_deg):
    return np.dot(rot_matrix(angle_deg), vec)

# Parsing command line options
parser = argparse.ArgumentParser(description='Generates simulation cases \
                                 for parameter study using PyFoam.')

parser.add_argument('--template_case', dest="template_case", type=str, 
                    help='OpenFOAM template case.', required=True)

parser.add_argument('--study_name', dest="study_name", type=str, 
                    help='Name of the parameter study.', required=True)

parser.add_argument('--alpha_min', dest="alpha_min", type=float, 
                    help='Minimal angle of attack in degrees.', 
                    required=True)

parser.add_argument('--alpha_max', dest="alpha_max", type=float, 
                    help='Maximal angle of attack in degrees.', 
                    required=True)

parser.add_argument('--n_alphas', dest="n_alphas", type=int, 
                    help='Number of angles between alpha_min and alpha_max.', 
                    required=True)

args = parser.parse_args()

if __name__ == '__main__':

    args = parser.parse_args(sys.argv[1:])

    # Distribute angles of attack 
    angles = np.linspace(args.alpha_min, args.alpha_max, args.n_alphas)

    # Initialize the template case 
    template_case = SolutionDirectory(args.template_case)
    # Initialize the parameter dictionary 
    param_dict = {}
    # For every variation 
    for variation_id,alpha in enumerate(angles):
        # Add variation-d : parameter vector to parameter dictionary.
        param_dict[variation_id] = {"ALPHA" : float(alpha)}
        # Clone the template case into a variation ID case. 
        name = "%s-%08d" % (args.study_name, variation_id) 
        case_name = os.path.join(os.path.curdir, name)
        cloned_case = template_case.cloneCase(case_name)
        # Read the velocity field file.
        u_file_path = os.path.join(cloned_case.name,"0", "U")
        u_file = ParsedParameterFile(u_file_path)
        # Read the velocity inlet boundary condition and internal field.
        u_inlet = u_file["boundaryField"]["INLET"]["value"]
        u_internal = u_file["internalField"]
        # Rotate the velocity clockwise by alpha around (-y). 
        u_numpy = np.array(u_inlet[1])
        u_numpy = rot_vector(u_numpy, alpha)
        # Set inlet and internal velocity.
        u_inlet.setUniform(Vector(u_numpy[0],u_numpy[1],u_numpy[2]))
        u_internal.setUniform(Vector(u_numpy[0],u_numpy[1],u_numpy[2]))
        u_file.writeFile()

    # Store variation ID : study parameters into a YAML file.
    with open(args.study_name + '.yml', 'w') as param_file:
        yaml.dump(param_dict, param_file, default_flow_style=False)