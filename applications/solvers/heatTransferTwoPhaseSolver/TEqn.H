Deff == alpha1*k1/Cv1 + (1.0-alpha1)*k2/Cv2;

solve
(
    fvm::ddt(rho, T)
    + fvm::div(rhoPhi, T)
    - fvm::laplacian(Deff, T)
);

